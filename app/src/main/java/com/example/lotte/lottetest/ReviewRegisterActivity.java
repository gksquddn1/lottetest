package com.example.lotte.lottetest;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lotte.lottetest.Network.RegisterReview;


public class ReviewRegisterActivity extends AppCompatActivity {

    ImageView review_register_backBtn;
    ImageView review_registerBtn;
    EditText review_store_name;
    TextView review_limit;
    EditText editText_review;
    String store_id;
    String store_name;
    String event_check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_register);



        setView();
        setListener();

    }

    void setView() {
        review_register_backBtn = (ImageView)findViewById(R.id.activity_review_register_backBtn);
        review_registerBtn = (ImageView)findViewById(R.id.activity_review_registerBtn);
        editText_review = (EditText)findViewById(R.id.edit_review);
        review_store_name = (EditText)findViewById(R.id.review_store_name);
        review_limit = (TextView)findViewById(R.id.text_limit_count);

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    void setListener() {
        review_register_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        review_registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 네트워크 연결해서 등록해주는 부분.
                String content = editText_review.getText().toString();
                String title = review_store_name.getText().toString();

                SharedPreferences login_pref = getSharedPreferences("Mylogin", MODE_PRIVATE);
                String user_id = login_pref.getString("user_id", null);
                String department = login_pref.getString("department", null);
                RegisterReview task = null;
                String encode_content = null;
                try {
                    encode_content = AES256Cipher.AES_Decode(content, InfoManager.key);
                } catch (Exception var8) {
                    var8.printStackTrace();
                }
                task = new RegisterReview(getApplicationContext(), user_id, department, encode_content, title);
                task.execute();
                setResult(RESULT_OK);
            }
        });

        editText_review.addTextChangedListener(new TextWatcher() {
            String strCur;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                strCur = s.toString();

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 200) {
                    editText_review.setText(strCur);
                    editText_review.setSelection(start);
                } else {
                    review_limit.setText(String.valueOf(s.length()));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
