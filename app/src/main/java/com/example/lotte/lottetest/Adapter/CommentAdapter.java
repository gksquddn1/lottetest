package com.example.lotte.lottetest.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lotte.lottetest.Model.Comment;
import com.example.lotte.lottetest.R;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by byungwoo on 2016-01-04.
 */
public class CommentAdapter extends BaseAdapter {
    Context context;
    private Comment comment;

    private ArrayList<Object> comm_item = new ArrayList<Object>();


    public CommentAdapter(Context context) {
        super();
        this.context = context;
    }

    @Override
    public int getCount() {
        return comm_item.size();
    }

    @Override
    public Object getItem(int position) {
        return comm_item.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        Context context = parent.getContext();

        if(comm_item.get(position) instanceof Comment) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_comment, null);

                holder = new ViewHolder();
                holder.id = (TextView) convertView.findViewById(R.id.id);
                holder.comment = (TextView) convertView.findViewById(R.id.comment);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            comment = (Comment) getItem(position);

            try {
                holder.id.setText("작성자 : " + URLDecoder.decode(comment.getId(), "utf-8"));
                holder.comment.setText(URLDecoder.decode(comment.getComment(), "utf-8"));

            } catch (Exception e) {
                Log.wtf("AroundStoreAdapter", e.toString());
                e.printStackTrace();
            }


        }

        return convertView;
    }

    public void add(Comment comment_info) {
        comm_item.add(comment_info);
    }

    private class ViewHolder {
        public TextView id;
        public TextView comment;
    }


}
