package com.example.lotte.lottetest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lotte.lottetest.Fragment.FragmentOne;
import com.example.lotte.lottetest.Fragment.FragmentThree;
import com.example.lotte.lottetest.Fragment.FragmentTwo;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private BackPressCloseHandler backPressCloseHandler;
    public static final int FRAGMENT_ONE = 0;
    public static final int FRAGMENT_TWO = 1;
    public static final int FRAGMENT_THREE = 2;
    static final int REQUEST_CODE = 6666;
    static final int LOGOUT_CODE = 4444;
    static final int AGAIN_GPS = 3333;
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;

    TabLayout tabLayout;
    RelativeLayout background_layout;
    ProgressDialog progressDialog;

    String user_id;
    String user_name;
    String user_department;
    NavigationView navigationView;

    // nav_bar
    LinearLayout nav_home;
    RelativeLayout nav_month_event;
    RelativeLayout nav_editor_rec;
    RelativeLayout nav_search_store;
    RelativeLayout nav_location_setting;
    LinearLayout nav_app_info;

    SharedPreferences pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        backPressCloseHandler = new BackPressCloseHandler(this);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        setView();
        setListener();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);


    }


    void setView() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        background_layout = (RelativeLayout)findViewById(R.id.background_layout);
        nav_home = (LinearLayout)findViewById(R.id.nav_home);
        nav_month_event = (RelativeLayout)findViewById(R.id.nav_month_event);
        nav_editor_rec = (RelativeLayout)findViewById(R.id.nav_editor_rec);
        nav_search_store = (RelativeLayout)findViewById(R.id.nav_search_store);
        nav_location_setting = (RelativeLayout)findViewById(R.id.nav_location_setting);
        nav_app_info = (LinearLayout)findViewById(R.id.nav_app_info);

        ImageView nav_image = (ImageView)findViewById(R.id.nav_image);
        TextView nav_nickname = (TextView)findViewById(R.id.nav_nickname);
        TextView nav_email = (TextView)findViewById(R.id.nav_email);
        pref = getSharedPreferences("Mylogin", MODE_PRIVATE);

        user_id = pref.getString("user_id", "1");
        user_department = pref.getString("department", null);
        user_name = pref.getString("name", null);

        if(user_id == null) {
            Log.e("ㅎㅎ", "아직 로그인 안됨.");

            nav_image.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.main_profile));
            nav_nickname.setText("이름");
            nav_email.setText("아이디");

        }
        else {
            Log.e("ㅎㅎ", "로그인.");
            // 유저의 정보를 가져와......
            nav_image.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.main_profile));
            nav_nickname.setText(user_name);
            nav_email.setText(user_id);
//            ReceiveUserInfo task = new ReceiveUserInfo(MainActivity.this, user_id, nav_image, nav_nickname, nav_email);
//            task.execute();
        }

    }

    void setListener() {
        background_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user_id == null ) { //&& !Session.getCurrentSession().checkAndImplicitOpen()) {
                    Intent intent = new Intent(getApplicationContext(), IntroLoginActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), PersonalActivity.class);
                    startActivityForResult(intent, LOGOUT_CODE);
                }
            }
        });
        nav_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0, false);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        nav_month_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1, false);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        nav_editor_rec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(2, false);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        nav_search_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
//                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
//                startActivity(intent);
            }
        });
        nav_location_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), Map_location.class);
//                startActivityForResult(intent, REQUEST_CODE);
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//                drawer.closeDrawer(GravityCompat.START);
            }
        });
        nav_app_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AppInfoActivity.class);
                startActivity(intent);
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            backPressCloseHandler.onBackPressed();
        }
    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Fragment newFragment = null;

            switch (position) {
                case FRAGMENT_ONE :
                    newFragment = new FragmentOne();
                    Bundle bundle = new Bundle();
                    bundle.putString("saved_user_id", user_id);
                    newFragment.setArguments(bundle);
                    break;
                case FRAGMENT_TWO:
                    newFragment = new FragmentTwo();
                    break;
                case FRAGMENT_THREE:
                    newFragment = new FragmentThree();
                    break;
                default:
                    break;
            }
            return newFragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);

            }
            return null;
        }


    }

}
