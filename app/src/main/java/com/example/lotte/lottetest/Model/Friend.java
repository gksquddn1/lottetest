package com.example.lotte.lottetest.Model;

import java.io.Serializable;

/**
 * Created by byungwoo on 2016-01-08.
 */
public class Friend implements Serializable {

    private static final long serialVersionUID = 4220461820168818967L;

    private String id;
    private String name;
    private String message;
    private String department;
    private String image;

    public Friend(String id, String name, String message, String department, String image) {
        this.id = id;
        this.name = name;
        this.message = message;
        this.department = department;
        this.image = image;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
