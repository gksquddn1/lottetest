package com.example.lotte.lottetest.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lotte.lottetest.Model.Friend;
import com.example.lotte.lottetest.R;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by byungwoo on 2016-01-04.
 */
public class FriendAdapter extends BaseAdapter {
    Context context;
    private Friend friend;

    private ArrayList<Object> friend_item = new ArrayList<Object>();


    public FriendAdapter(Context context) {
        super();
        this.context = context;
    }

    @Override
    public int getCount() {
        return friend_item.size();
    }

    @Override
    public Object getItem(int position) {
        return friend_item.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        Context context = parent.getContext();

        if(friend_item.get(position) instanceof Friend) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_friend, null);

                holder = new ViewHolder();
                holder.pic_img = (ImageView) convertView.findViewById(R.id.store_image);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                holder.id = (TextView) convertView.findViewById(R.id.id);
                holder.department = (TextView) convertView.findViewById(R.id.department);
                holder.message = (TextView) convertView.findViewById(R.id.message);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            friend = (Friend) getItem(position);

            try {

                if(friend.getImage() != null) {
                    Glide.with(context).load(friend.getImage()).into(holder.pic_img);
                } else {
                    holder.pic_img.setImageDrawable(context.getResources().getDrawable(R.drawable.no_picture));
                }

                holder.name.setText(URLDecoder.decode(friend.getName(), "utf-8"));
                holder.department.setText(URLDecoder.decode(friend.getDepartment(), "utf-8"));
                holder.id.setText(URLDecoder.decode(friend.getId(), "utf-8"));
                holder.message.setText(URLDecoder.decode(friend.getMessage(), "utf-8"));

//
//                if(friend.getDistance() != null && !friend.getDistance().equals("null")) {
//                    double temp;
//                    temp = Double.parseDouble(friend.getDistance());
//                    int temp2 = (int) (temp * 100.0);
//                    temp = temp2 / 100.0;
//
//                    holder.store_distance.setText(temp + "km");
//                } else {
//                    holder.store_distance.setText("");
//                }


            } catch (Exception e) {
                Log.wtf("AroundStoreAdapter", e.toString());
                e.printStackTrace();
            }


        }

        return convertView;
    }

    public void add(Friend friend_info) {
        friend_item.add(friend_info);
    }

    private class ViewHolder {
        public ImageView pic_img;
        public TextView name;
        public TextView id;
        public TextView department;
        public TextView message;
    }


}
