package com.example.lotte.lottetest.Network;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.lotte.lottetest.InfoManager;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;

/**
 * Created by byungwoo on 2016-01-06.
 */
public class ReceiveLogin extends AsyncTask<String, Integer, String> {


    Context context;
    ////////////////////////////////////////////

    String json;

    String user_id;
    String password;

    public ReceiveLogin(Context context, String user_id, String password) {
        this.context = context;
        this.user_id = user_id;
        this.password = password;

    }

    @Override
    protected String doInBackground(String... params) {
        // 연결 시작


//        String url = "http://wkdgusdn3.dothome.co.kr/mamimap/review_list.php?store_id="+store_id;
        String url = InfoManager.url + "login.php?id="+user_id+"&password="+password;

        OkHttpClient client = new OkHttpClient();

        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            json = response.body().string();    // 받아온 것 간순히 넣어주는 중..

//            Log.e("json", json);
        } catch (Exception e) {
//            Log.e("여기서?", e.toString());
            e.printStackTrace();
        }
        //Toast.makeText(context, "좌표" + saved_latitude + " : " + saved_longitude, Toast.LENGTH_SHORT).show();
        return json;
    }

    @Override
    protected void onPostExecute(String s) {
//        Log.e("JSON--", user_id+":"+password);
//        Log.e("JSON", s);
//        Log.e("JSON", json);
        String state=null;
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray friendInfo = jsonObject.getJSONArray("login");


            for(int i=0; i<friendInfo.length(); i++) {
                JSONObject infoObject = friendInfo.getJSONObject(i);

                state = URLDecoder.decode(infoObject.getString("state"), "utf-8");
                String user_id = URLDecoder.decode(infoObject.getString("id"), "utf-8");
                String department = URLDecoder.decode(infoObject.getString("department"), "utf-8");
                String name = URLDecoder.decode(infoObject.getString("name"), "utf-8");
                String message = URLDecoder.decode(infoObject.getString("message"), "utf-8");
//                String image = URLDecoder.decode(infoObject.getString("image"), "utf-8");


                SharedPreferences login_info = context.getSharedPreferences("Mylogin", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = login_info.edit();
                editor.putString("user_id", user_id);
                editor.putString("department", department);
                editor.putString("name", name);
                editor.putString("message", message);
//                editor.putString("image", image);
                editor.commit();
//            Log.e("user_id---------여기서 받음", id);
//            SharedPreferences pref = context.getSharedPreferences("Mylogin", Context.MODE_PRIVATE);
//            String user = pref.getString("user_id", "a");
//            Log.e("로그인..", user);
            }




        } catch (Exception e) {
            e.printStackTrace();
        }


        if(state.equals("success")) {
            Toast.makeText(context, "로그인 했습니다.", Toast.LENGTH_SHORT).show();
            InfoManager.isLogin = true;
            InfoManager.activity.finish();
            InfoManager.pre_activity.finish();

        } else {
//
//            if(Session.getCurrentSession().checkAndImplicitOpen()) {
//                // 카카오 로그인 처음인 경우..
//                RegisterMember task = new RegisterMember(context,user_id, null);
//                task.execute();
//            }

            Toast.makeText(context, "로그인에 실패했습니다.", Toast.LENGTH_SHORT).show();

        }

    }
}
