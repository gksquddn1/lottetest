package com.example.lotte.lottetest.Model;

import java.io.Serializable;

/**
 * Created by byungwoo on 2016-01-08.
 */
public class Review implements Serializable {

    private static final long serialVersionUID = 4220461820168818967L;

    private String review_id;
    private String title;
    private String content;
    private String department;
    private String id;

    public Review(String review_id, String title, String content, String department, String id) {

        this.review_id = review_id;
        this.title = title;
        this.content = content;
        this.department = department;
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
