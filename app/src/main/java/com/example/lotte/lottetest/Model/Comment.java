package com.example.lotte.lottetest.Model;

import java.io.Serializable;

/**
 * Created by byungwoo on 2016-01-08.
 */
public class Comment implements Serializable {

    private static final long serialVersionUID = 4220461820168818967L;

    private String id;
    private String comment;

    public Comment(String id, String comment) {
        this.id = id;
        this.comment = comment;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
