package com.example.lotte.lottetest.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lotte.lottetest.Network.ReceiveFriend;
import com.example.lotte.lottetest.R;


/**
 * Created by byungwoo on 2015-12-30.
 */
public class FragmentOne extends Fragment {

    View view;
    private ListView list;
    TextView no_people;
    ReceiveFriend task;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle saveInstanceState) {
        view =  inflater.inflate(R.layout.fragment_one, null);



        setView();

        Bundle data = getArguments();
        String string_id = data.getString("saved_user_id");

        task = new ReceiveFriend(view.getContext(), list, string_id, no_people);
        task.execute();


        setListener();

        Log.e("FRAGMENTONE", "시작!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        return view;
    }

    void setView() {
        list = (ListView)view.findViewById(R.id.around_store);
        no_people = (TextView)view.findViewById(R.id.noStore);
    }

    void setListener() {

        // scrollview 안에 있는 listview touch
//        list.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                fragone_scroll.requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });
//
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                InfoManager.page = 1;
//                Intent intent = new Intent(view.getContext(), StoreActivity.class);
//                StoreForm storeInfo = (StoreForm) parent.getAdapter().getItem(position);
//                intent.putExtra("SELECT", "around");
//                intent.putExtra("STOREINFO", storeInfo);
//                startActivity(intent);
//
//            }
//        });
    }

}
