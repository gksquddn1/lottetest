package com.example.lotte.lottetest.Network;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.lotte.lottetest.InfoManager;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.net.URLDecoder;

/**
 * Created by byungwoo on 2016-01-06.
 */
public class KakaoLogin extends AsyncTask<String, Integer, String> {


    Context context;
    ////////////////////////////////////////////

    String json;

    String id;

    public KakaoLogin(Context context, String id) {
        this.context = context;
        this.id = id;
    }

    @Override
    protected String doInBackground(String... params) {
        // 연결 시작


//        String url = "http://wkdgusdn3.dothome.co.kr/mamimap/review_list.php?store_id="+store_id;
        String url = InfoManager.url + "user/kakao_login";

        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody body = new FormEncodingBuilder()
                    .add("id_email", id)
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();

            Response response = client.newCall(request).execute();
            json = response.body().string();    // 받아온 것 간순히 넣어주는 중..

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Toast.makeText(context, "좌표" + saved_latitude + " : " + saved_longitude, Toast.LENGTH_SHORT).show();
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        String state=null;
        try {
            JSONObject jsonObject = new JSONObject(json);


            state = URLDecoder.decode(jsonObject.getString("state"), "utf-8");
            String id = URLDecoder.decode(jsonObject.getString("id"), "utf-8");


            SharedPreferences pref = context.getSharedPreferences("Mylogin", context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("user_id", id);
            editor.commit();


        } catch (Exception e) {
            e.printStackTrace();
        }

        if(state.equals("success")) {
            InfoManager.isLogin = true;
            InfoManager.pre_activity.finish();
            InfoManager.activity.finish();
        } else {
            Toast.makeText(context, "로그인에 실패하셨습니다.", Toast.LENGTH_SHORT).show();
        }

    }
}
