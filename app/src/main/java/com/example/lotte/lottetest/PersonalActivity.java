package com.example.lotte.lottetest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kakao.auth.Session;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;


public class PersonalActivity extends AppCompatActivity {

    static final int CHANGE_INFO = 3333;

    ImageView person_backBtn;
    ImageView person_change;
    ImageView person_password;
    ImageView person_logout;

    ImageView person_image;
    TextView person_nickname;
    TextView person_email;
    TextView person_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal);

        SharedPreferences user_info = getSharedPreferences("USERINFO", MODE_PRIVATE);
        String user_image = user_info.getString("image", null);
        String user_nickname = user_info.getString("nickname", null);
        String user_email = user_info.getString("id_email", null);
        String user_statusMessage = user_info.getString("status_message", null);

        setView();

        Log.e("PersonAc", user_image+" : "+user_nickname+" : "+user_statusMessage);

        // 그냥 회원으로 가입했을 경우
        Glide.with(getApplicationContext()).load(user_image).into(person_image);

        person_nickname.setText(user_nickname);

        if(Session.getCurrentSession().checkAndImplicitOpen()) {
            person_email.setText("");
            person_email.setVisibility(View.GONE);
        } else {
            person_email.setText(user_email);
        }

        if(user_statusMessage == null || user_statusMessage.equals("null")) {
            person_status.setText("");
        } else {
            person_status.setText(user_statusMessage);
        }

        setListener();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case CHANGE_INFO :
                SharedPreferences user_info = getSharedPreferences("USERINFO", MODE_PRIVATE);
                String user_image = user_info.getString("image", null);
                String user_nickname = user_info.getString("nickname", null);
                String user_email = user_info.getString("id_email", null);
                String user_statusMessage = user_info.getString("status_message", null);

                Log.e("image", "user_image:"+user_image);
                Glide.with(getApplicationContext()).load(user_image).into(person_image);

                person_nickname.setText(user_nickname);
                person_email.setText(user_email);
                if(user_statusMessage == null || user_statusMessage.equals("null")) {
                    person_status.setText("");
                } else {
                    person_status.setText(user_statusMessage);
                }

                break;
        }

    }

    void setView() {
        person_backBtn = (ImageView)findViewById(R.id.activity_person_backBtn);
        person_change = (ImageView)findViewById(R.id.person_change);
        person_password = (ImageView)findViewById(R.id.person_password);
        person_logout = (ImageView)findViewById(R.id.person_logout);
        person_image = (ImageView)findViewById(R.id.person_image);
        person_nickname = (TextView)findViewById(R.id.person_nickname);
        person_email = (TextView)findViewById(R.id.person_email);
        person_status = (TextView)findViewById(R.id.person_status);
    }


    void setListener() {
        person_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        person_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 정보수정버튼
//                Intent intent = new Intent(getApplicationContext(), InformationChangeActivity.class);
//                startActivityForResult(intent, CHANGE_INFO);
            }
        });

        person_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 비밀번호 변경
                SharedPreferences login_pref = getSharedPreferences("Mylogin", MODE_PRIVATE);
                String user_id = login_pref.getString("user_id", null);
                if(Session.getCurrentSession().checkAndImplicitOpen()) {
                    Toast.makeText(getApplicationContext(), "카톡 회원은 비밀번호가 없습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }

//                Intent intent = new Intent(getApplicationContext(), PasswordChangeActivity.class);
//                startActivity(intent);
            }
        });

        person_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 로그아웃
                UserManagement.requestLogout(new LogoutResponseCallback() {
                    @Override
                    public void onCompleteLogout() {
                        Log.e("로그아웃", "됨");
                    }
                });
                // 회원 탈퇴를 하는 방법
//                UserManagement.requestUnlink(new UnLinkResponseCallback() {
//                    @Override
//                    public void onFailure(ErrorResult errorResult) {
//                    }
//
//                    @Override
//                    public void onSessionClosed(ErrorResult errorResult) {
//                    }
//
//                    @Override
//                    public void onNotSignedUp() {
//                    }
//                    @Override
//                    public void onSuccess(Long result) {
//                    }
//                });

                SharedPreferences user_info = getSharedPreferences("USERINFO", MODE_PRIVATE);
                user_info.edit().clear().commit();
                SharedPreferences login_pref = getSharedPreferences("Mylogin", MODE_PRIVATE);
                login_pref.edit().clear().commit();

                setResult(4444);
                finish();
            }
        });
    }


}
