package com.example.lotte.lottetest.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lotte.lottetest.Model.Review;
import com.example.lotte.lottetest.R;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by byungwoo on 2016-01-04.
 */
public class ReviewAdapter extends BaseAdapter {
    Context context;
    private Review review;

    private ArrayList<Object> review_item = new ArrayList<Object>();


    public ReviewAdapter(Context context) {
        super();
        this.context = context;
    }

    @Override
    public int getCount() {
        return review_item.size();
    }

    @Override
    public Object getItem(int position) {
        return review_item.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        Context context = parent.getContext();

        if(review_item.get(position) instanceof Review) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_review, null);

                holder = new ViewHolder();
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.id = (TextView) convertView.findViewById(R.id.id);
                holder.content = (TextView) convertView.findViewById(R.id.content);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            review = (Review) getItem(position);

            try {

//                if(friend.getImage() != null) {
//                    Glide.with(context).load(friend.getImage()).into(holder.pic_img);
//                } else {
//                    holder.pic_img.setImageDrawable(context.getResources().getDrawable(R.drawable.no_picture));
//                }

                holder.title.setText(URLDecoder.decode(review.getTitle(), "utf-8"));
                holder.content.setText(URLDecoder.decode(review.getContent(), "utf-8"));
                holder.id.setText(URLDecoder.decode(review.getId(), "utf-8"));

//
//                if(friend.getDistance() != null && !friend.getDistance().equals("null")) {
//                    double temp;
//                    temp = Double.parseDouble(friend.getDistance());
//                    int temp2 = (int) (temp * 100.0);
//                    temp = temp2 / 100.0;
//
//                    holder.store_distance.setText(temp + "km");
//                } else {
//                    holder.store_distance.setText("");
//                }


            } catch (Exception e) {
                Log.wtf("AroundStoreAdapter", e.toString());
                e.printStackTrace();
            }


        }

        return convertView;
    }

    public void add(Review review_info) {
        review_item.add(review_info);
    }

    private class ViewHolder {
        public TextView title;
        public TextView id;
        public TextView content;
    }


}
