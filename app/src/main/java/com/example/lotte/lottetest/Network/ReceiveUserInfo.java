package com.example.lotte.lottetest.Network;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lotte.lottetest.InfoManager;
import com.kakao.auth.Session;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.net.URLDecoder;

import static com.kakao.usermgmt.StringSet.nickname;

/**
 * Created by byungwoo on 2016-01-06.
 */
public class ReceiveUserInfo extends AsyncTask<String, Integer, String> {


    Context context;
    ////////////////////////////////////////////

    String json;
    String id;
    ImageView nav_image;
    TextView nav_nickname;
    TextView nav_email;

    public ReceiveUserInfo(Context context, String id, ImageView nav_image, TextView nav_nickname, TextView nav_email) {
        this.context = context;
        this.id = id;
        this.nav_image = nav_image;
        this.nav_nickname = nav_nickname;
        this.nav_email = nav_email;
    }

    public ReceiveUserInfo(Context context, String id) {
        this.context = context;
        this.id = id;
        this.nav_image = null;
        this.nav_nickname = null;
        this.nav_email = null;
    }

    @Override
    protected String doInBackground(String... params) {
        // 연결 시작

//        String url = "http://wkdgusdn3.dothome.co.kr/mamimap/user_info.php";
        String url = InfoManager.url + "info.php?id="+id;

        OkHttpClient client = new OkHttpClient();


        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            json = response.body().string();    // 받아온 것 간순히 넣어주는 중..

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Toast.makeText(context, "좌표" + saved_latitude + " : " + saved_longitude, Toast.LENGTH_SHORT).show();
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            Log.e("USER_json", json);
            JSONObject jsonObject = new JSONObject(json);


            if(jsonObject.length() == 0) {
                // 가입한 정보가 없었음.
                return;
            }


            String id = URLDecoder.decode(jsonObject.getString("id"), "utf-8");
            String name = URLDecoder.decode(jsonObject.getString("name"), "utf-8");
            String department = URLDecoder.decode(jsonObject.getString("department"), "utf-8");
            String message = URLDecoder.decode(jsonObject.getString("message"), "utf-8");
            String image = URLDecoder.decode(jsonObject.getString("image"), "utf-8");


            SharedPreferences pref = context.getSharedPreferences("login", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("id", id);
            editor.putString("name", name);
            editor.putString("department", department);
            editor.putString("image", image);
            editor.putString("message", message);
            editor.commit();

            if(nav_image != null && nav_nickname != null && nav_email != null) {
                try {
                    Log.e("ReceiveUserInfo", image + ":" + nickname + ":" + name + ":");
                    if (image != null && !image.equals("null")) {
                        Glide.with(context).load(image).into(nav_image);
                        Log.e("리시브유저인포", "이미지 테스트");

                    } else {
                        nav_image.setBackgroundColor(Color.rgb(0, 0, 0));
                    }
                    Log.e("ReceiveUserInfo_Check", nickname + ":" + name);
//                Glide.with(MainActivity.this).load(user_image).into(nav_image);
                    nav_nickname.setText(URLDecoder.decode(name, "utf-8"));
                    if(Session.getCurrentSession().checkAndImplicitOpen()) {
                        nav_email.setText("");
                    } else {
                        nav_email.setText(URLDecoder.decode(id, "utf-8"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
