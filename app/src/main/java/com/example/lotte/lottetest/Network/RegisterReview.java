package com.example.lotte.lottetest.Network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.lotte.lottetest.InfoManager;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.net.URLDecoder;

public class RegisterReview extends AsyncTask<String, Integer, String> {


    Context context;
    ////////////////////////////////////////////

    String json;

    String id;
    String department;
    String content;
    String title;

    public RegisterReview(Context context, String id, String department, String content, String title) {
        this.context = context;
        this.id = id;
        this.department = department;
        this.content = content;
        this.title = title;
    }


    @Override
    protected String doInBackground(String... params) {
        // 연결 시작


//        String url = "http://wkdgusdn3.dothome.co.kr/mamimap/review_list.php?store_id="+store_id;
        String url = null;
        RequestBody body = null;

        url = InfoManager.url + "register_review.php?id=1"+"&department=개발부&content="+content+"&title="+title;


        OkHttpClient client = new OkHttpClient();

        try {

            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            json = response.body().string();    // 받아온 것 간순히 넣어주는 중..

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Toast.makeText(context, "좌표" + saved_latitude + " : " + saved_longitude, Toast.LENGTH_SHORT).show();
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        String state=null;
        try {
            JSONObject jsonObject = new JSONObject(json);


            state = URLDecoder.decode(jsonObject.getString("state"), "utf-8");

        } catch (Exception e) {
            e.printStackTrace();
        }

        if(state.equals("success")) {
            Toast.makeText(context, "리뷰가 등록 되었습니다.", Toast.LENGTH_SHORT).show();
            Log.e("리뷰등록네트웤", "리뷰 true~");
            InfoManager.isReview = true;

        } else {
            Toast.makeText(context, "리뷰 등록에 실패했습니다.", Toast.LENGTH_SHORT).show();
        }
        InfoManager.activity.finish();

    }
}
