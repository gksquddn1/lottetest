package com.example.lotte.lottetest.Network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lotte.lottetest.Adapter.ReviewAdapter;
import com.example.lotte.lottetest.InfoManager;
import com.example.lotte.lottetest.Model.Review;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;

/**
 * Created by byungwoo on 2016-01-06.
 */
public class SendReview extends AsyncTask<String, Integer, String> {


    Context context;
    ////////////////////////////////////////////
    ReviewAdapter reviewAdapter;

    String json;
    ListView list;
    String part;
    TextView result;

    public SendReview(Context context, ListView list, String part, TextView result) {
        this.context = context;
        this.list = list;
        this.part = part;
        this.result = result;
    }

    @Override
    protected String doInBackground(String... params) {
        // 연결 시작

        reviewAdapter = new ReviewAdapter(context);

//        String url = "http://wkdgusdn3.dothome.co.kr/mamimap/around_store.php?latitude="+lati+"&longitude="+longi;
        //String url = "http://wkdgusdn3.dothome.co.kr/mamimap/around_store.php";
        String url = InfoManager.url + "review.php?part=" + part;

        OkHttpClient client = new OkHttpClient();


        try {

            Request request = new Request.Builder()
                    .url(url)
                    .build();
//            Request request = new Request.Builder()
//                    .url(url)
//                    .build();

            Response response = client.newCall(request).execute();
            json = response.body().string();    // 받아온 것 간순히 넣어주는 중..
            Log.e("TAG", "????왜지"+json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Toast.makeText(context, "좌표" + saved_latitude + " : " + saved_longitude, Toast.LENGTH_SHORT).show();
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        Log.e("TAG", json+"?????????");
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray reviewInfo = jsonObject.getJSONArray("review");

            if(reviewInfo.length() == 0) {
                list.setVisibility(View.GONE);
                result.setVisibility(View.VISIBLE);
            }

            for(int i=0; i<reviewInfo.length(); i++) {
                JSONObject infoObject = reviewInfo.getJSONObject(i);

                String id = URLDecoder.decode(infoObject.getString("id"), "utf-8");
                String review_id = URLDecoder.decode(infoObject.getString("review_id"), "utf-8");
                String department = URLDecoder.decode(infoObject.getString("department"), "utf-8");
                String title = URLDecoder.decode(infoObject.getString("title"), "utf-8");
                String content = URLDecoder.decode(infoObject.getString("content"), "utf-8");

                Log.e("TAG", id+review_id+department+title+content);
                Review receive_Info = new Review(review_id, title, content, department, id);

                reviewAdapter.add(receive_Info);

            }

        } catch (Exception e) {
            Log.e("ReceiveAround", e.toString());
            e.printStackTrace();
        }

        list.setAdapter(reviewAdapter);
    }

}
