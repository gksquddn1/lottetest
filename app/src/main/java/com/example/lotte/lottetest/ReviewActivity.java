package com.example.lotte.lottetest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lotte.lottetest.Model.Review;
import com.example.lotte.lottetest.Network.ReceiveComment;

public class ReviewActivity extends AppCompatActivity {

    ImageView activity_backBtn, activity_review_registerBtn;
    TextView title, content, noComment;
    Review reviewInfo;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        reviewInfo = (Review) getIntent().getSerializableExtra("REVIEWINFO");

        setView();
        setListener();
    }
    void setView() {
        activity_backBtn = (ImageView)findViewById(R.id.activity_backBtn);
        title = (TextView)findViewById(R.id.title);
        content = (TextView)findViewById(R.id.content);
        activity_review_registerBtn = (ImageView)findViewById(R.id.activity_review_registerBtn);
        list = (ListView)findViewById(R.id.comment_list);
        noComment = (TextView)findViewById(R.id.noComment);

        ReceiveComment task = new ReceiveComment(getApplicationContext(), list, reviewInfo.getReview_id(), noComment);
        task.execute();

        title.setText(reviewInfo.getTitle());
        content.setText(reviewInfo.getContent());



    }
    void setListener() {
        activity_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        activity_review_registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 댓글 등록 페이지로 이동
            }
        });
    }
}

