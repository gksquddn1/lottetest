package com.example.lotte.lottetest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotte.lottetest.Network.ReceiveLogin;

import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class LoginActivity extends AppCompatActivity {

    EditText email_address;
    EditText password;
    TextView find_password;
    ImageView login_Btn;
    FindPassword_CustomDialog findPassword_customDialog;


//    private SessionCallback callback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        InfoManager.pre_activity = this;

//        callback = new SessionCallback();
//        Session.getCurrentSession().addCallback(callback);
        setContentView(R.layout.activity_login);

        setView();
        setListener();


    }



    void setView() {
        email_address = (EditText) findViewById(R.id.login_email);
        password = (EditText) findViewById(R.id.login_password);
        find_password = (TextView) findViewById(R.id.find_mypassword);
        login_Btn = (ImageView) findViewById(R.id.login_Btn);
    }

    void setListener() {
        find_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPassword_customDialog = new FindPassword_CustomDialog(LoginActivity.this, mSendClickListener, mCancelClickListener);
                findPassword_customDialog.show();
            }
        });

        login_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (email_address.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "이메일 입력해주세요", Toast.LENGTH_SHORT).show();
                    return;
                } else if (password.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "비밀번호를 입력해주세요", Toast.LENGTH_SHORT).show();
                    return;
                }


                String hash_sha256 = SHA_256(password.getText().toString());




                ReceiveLogin task = new ReceiveLogin(LoginActivity.this, email_address.getText().toString(), hash_sha256);
                task.execute();
            }
        });
    }

    private View.OnClickListener mSendClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            EditText editText_email = (EditText) findPassword_customDialog.findViewById(R.id.write_email);
            String email = editText_email.getText().toString();
            if(editText_email.getText().toString().equals("")) {
                Toast.makeText(LoginActivity.this, "이메일을 입력해주세요", Toast.LENGTH_SHORT).show();
            } else {
//                RegisterSendEmail task = new RegisterSendEmail(LoginActivity.this, email);
//                task.execute();

                findPassword_customDialog.dismiss();
            }

        }
    };

    private View.OnClickListener mCancelClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            findPassword_customDialog.dismiss();

        }
    };

    String SHA_256(String password) {
        String hash = null;
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(password.getBytes());

            hash = bytesToHexString(digest.digest());

            Log.e("SHA-256?", "result:" + hash);


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


        return hash;
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    String rsa_function(String password) {

        PublicKey publicKey = null;

        String pubKeyPEM = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqhpRFGKszGyraNLRGshRx88d7\n" +
                "MExNW3O6kD12wQhj26haBsF9DX+LLQGtEM0kr/yvja6WEhvHAga0g2BDjle4M2E7\n" +
                "BvFo44f0ibUwgobxJkMQg6wrxWiR8LmhM60Wi/uiUPlgt7ZPtf9SqDmIFDloX3Xt\n" +
                "RuWD/0ADoKzBweQnbQIDAQAB";

        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            publicKey = kf.generatePublic(new X509EncodedKeySpec(Base64.decode(pubKeyPEM, Base64.DEFAULT)));

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (publicKey != null) {
            Log.e("?", "publicKey : " + publicKey.toString());
        }

        // Encode
        byte[] encodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
            c.init(Cipher.ENCRYPT_MODE, publicKey);
            encodedBytes = c.doFinal(password.getBytes());
        } catch (Exception e) {
            Log.e("TAG", "RSA encryption error");
            e.printStackTrace();
        }

        return Base64.encodeToString(encodedBytes, Base64.DEFAULT);
    }

//    public static byte[] decrypt(String key) throws Exception {
//        String privKeyPEM =
//                "MIICXQIBAAKBgQCqhpRFGKszGyraNLRGshRx88d7MExNW3O6kD12wQhj26haBsF9\n" +
//                        "DX+LLQGtEM0kr/yvja6WEhvHAga0g2BDjle4M2E7BvFo44f0ibUwgobxJkMQg6wr\n" +
//                        "xWiR8LmhM60Wi/uiUPlgt7ZPtf9SqDmIFDloX3XtRuWD/0ADoKzBweQnbQIDAQAB\n" +
//                        "AoGAWwsLMnDfRqPklye4cF43iDdtc5nvwwVmnl7QUgzxm8707UvaFhgU1ouq72me\n" +
//                        "/U1wjZIJeloktnWgetQBm0/uCKzlLAMoc2qOBtLmFwuMVk0TbKb8C3BFC/z1adng\n" +
//                        "5eXuqBIc6i/zQyMOalZaFeHhVCkb6+HqJk74CR4plc2kbsECQQDezMrGjegGeDP+\n" +
//                        "z4KW2uWSdXU+4Pd3hbz7qBjNwmMs6MhKCDWRUTW77aU0cxEXdd44eaIkBmuK68qD\n" +
//                        "3cIRtcKpAkEAw++qw/aHcN78VwC02/JE1vCyphxxN9S5WGKmnrN2sqqfX5g+PUja\n" +
//                        "MLnEtnQf5LpdV/XzRIqLvNitmjiEDA79JQJBAJ/PBSOmZec+EQ2UinbqTADhf1e6\n" +
//                        "xcZVh1h7Gk98PubXkdKIUOQK/B/wNa+JWZkaUmrv0/1DHaBnwLsVNT6+wQECQQC6\n" +
//                        "QWsIS6LmpeIkZnSGnJ/Z3wdEJsdtdlcWcPjI5fqQzucffV607TsUlJMtiegDizFO\n" +
//                        "e2vcHQPG6gblEpJb0CfNAkAsYqjE+UxcCj5itZW07JmqFMYmXdNjnVbJ73ihE3S5\n" +
//                        "8xhLbHPGSgvr00GD8lg/eYuBRZnw7J6liLzdf41k+0JM";
//
//
//        byte [] encoded = Base64.decode(privKeyPEM, Base64.DEFAULT);
//
//
//        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
//        KeyFactory kf = KeyFactory.getInstance("RSA");
//        PrivateKey privKey = kf.generatePrivate(keySpec);
//
//        Cipher cipher = Cipher.getInstance("RSA", "BC");
//        cipher.init(Cipher.DECRYPT_MODE, privKey);
//
//        byte[] decodedStr           = Base64.decode(key, Base64.DEFAULT);
//        byte[] plainText            = cipher.doFinal(decodedStr);
//
//
//        Log.e("해제", plainText+"");
//        Log.e("해제2", Base64.encodeToString(plainText, Base64.DEFAULT));
//        Log.e("해제3", new String(plainText));
//        return plainText;
//    }
//

    /////////////////////////////////////////////////////////////////////////////
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
//            return;
//        }
//
//        super.onActivityResult(requestCode, resultCode, data);
//    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        Session.getCurrentSession().removeCallback(callback);
//    }
//
//
//    private class SessionCallback implements ISessionCallback {
//
//        @Override
//        public void onSessionOpened() {
//
//             세션이 연결이 되었다!
//            Toast.makeText(getApplicationContext(), "로그인 되었습니다.", Toast.LENGTH_SHORT).show();
//            requestMe();
//        }
//
//        @Override
//        public void onSessionOpenFailed(KakaoException exception) {
//            if (exception != null) {
//                Logger.e(exception);
//            }
//            setContentView(R.layout.activity_login);
//        }
//    }
//
//    void requestMe() {
//
//        UserManagement.requestMe(new MeResponseCallback() {
//            @Override
//            public void onFailure(ErrorResult errorResult) {
//                Log.e("TAG", "[requestMe]onFailure");
//
//                String message = "failed to get user info. msg=" + errorResult;
//
//                ErrorCode result = ErrorCode.valueOf(errorResult.getErrorCode());
//                if (result == ErrorCode.CLIENT_ERROR_CODE) {
//                    Log.e("TAG", "[requestMe]ErrorCode.CLIENT_ERROR_CODE : " + message);
//                } else {
//                    Log.e("TAG", "[requestMe]Error : " + message);
//                }
//            }
//
//            @Override
//            public void onSessionClosed(ErrorResult errorResult) {
//                Log.e("TAG", "[requestMe]onSessionClosed");
//
//            }
//
//            @Override
//            public void onSuccess(UserProfile userProfile) {
//                Log.e("TAG", "[requestMe]onSuccess");
//                //유저 정보 요청 성공.
//                //userProfile.toString() 전체 정보를 볼수있고
//                userProfile.saveUserToCache();
//
//                Log.e("LoginActivity", "카톡 로그인 : " + userProfile.getId());
//
//
//                KakaoLogin task = new KakaoLogin(getApplicationContext(), Long.toString(userProfile.getId()));
//                task.execute();
//                //userProfile.getId() 를 사용하여 Unique ID값을 얻어낼수 있다.
//            }
//
//            @Override
//            public void onNotSignedUp() {
//                Log.e("TAG", "[requestMe]onNotSignedUp");
//            }
//        });
//    }
}