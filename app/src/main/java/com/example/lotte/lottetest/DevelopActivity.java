package com.example.lotte.lottetest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lotte.lottetest.Model.Review;
import com.example.lotte.lottetest.Network.ReceiveReview;

public class DevelopActivity extends AppCompatActivity {

    TextView part, nolist;
    ListView list;
    ReceiveReview receiveReview;
    RelativeLayout review_register;
    ImageView activity_backBtn;
    static final int reStart = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_develop);

        setView();
        setListener();
    }

    void setView() {
        part = (TextView)findViewById(R.id.part);
        nolist = (TextView)findViewById(R.id.nolist);
        list = (ListView)findViewById(R.id.Sales_list);
        activity_backBtn = (ImageView)findViewById(R.id.activity_backBtn);
        review_register = (RelativeLayout)findViewById(R.id.review_register);
        part.setText("개발부");
        receiveReview = new ReceiveReview(getApplicationContext(), list, "개발부", nolist);
        receiveReview.execute();
    }
    void setListener() {
        activity_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        review_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences login_pref = getSharedPreferences("Mylogin", MODE_PRIVATE);
                String user_id = login_pref.getString("user_id", null);


//                if (user_id == null && !Session.getCurrentSession().checkAndImplicitOpen()) {
//                    // 로그인이 안되었을 경우
//                    Intent intent = new Intent(SalesActivity.this, IntroLoginActivity.class);
//                    startActivity(intent);
//
//
//                } else {
                Intent intent = new Intent(getApplicationContext(), ReviewRegisterActivity.class);
                startActivity(intent);
//                    intent.putExtra("STOREID_2", store_id);
//                    intent.putExtra("STORE_NAME2", store_name);
//                    if (event_check.equals("0")) {
//                        intent.putExtra("EVENT", "0");
//                    } else {
//                        intent.putExtra("EVENT", "1");
//                    }
//                    startActivityForResult(intent, reStart);


            }
//            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InfoManager.page = 1;
                Intent intent = new Intent(view.getContext(), ReviewActivity.class);
                Review reviewInfo = (Review) parent.getAdapter().getItem(position);
//                intent.putExtra("SELECT", "sales");
                intent.putExtra("REVIEWINFO", reviewInfo);
                startActivity(intent);

            }
        });

    }

}
