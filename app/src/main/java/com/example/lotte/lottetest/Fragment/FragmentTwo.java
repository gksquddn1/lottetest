package com.example.lotte.lottetest.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lotte.lottetest.DevelopActivity;
import com.example.lotte.lottetest.ManageActivity;
import com.example.lotte.lottetest.R;
import com.example.lotte.lottetest.SalesActivity;
import com.example.lotte.lottetest.SellerActivity;


/**
 * Created by byungwoo on 2015-12-30.
 */
public class FragmentTwo extends Fragment {

    View view;
    TextView sales, manage, develop, seller;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle saveInstanceState) {
        view = inflater.inflate(R.layout.fragment_two, null);

        setView();
        setListener();
        return view;
    }

    void setView() {
        sales = (TextView)view.findViewById(R.id.sales);
        manage =(TextView)view.findViewById(R.id.manage);
        develop =(TextView)view.findViewById(R.id.develop);
        seller =(TextView)view.findViewById(R.id.seller);
    }
    void setListener() {

        sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), SalesActivity.class);
                startActivity(intent);

            }
        });

        manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), ManageActivity.class);
                startActivity(intent);

            }
        });

        develop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), DevelopActivity.class);
                startActivity(intent);

            }
        });
        seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), SellerActivity.class);
                startActivity(intent);

            }
        });

    }


//    private void setGridView() {
//
//
//
//        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                InfoManager.page = 2;
//                Intent intent = new Intent(view.getContext(), StoreActivity.class);
//                MonthEvent monthInfo = (MonthEvent)parent.getAdapter().getItem(position);
//                intent.putExtra("SELECT", "event");
//                intent.putExtra("MONTHINFO", monthInfo);
//                startActivity(intent);
//            }
//        });
//    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }
}
