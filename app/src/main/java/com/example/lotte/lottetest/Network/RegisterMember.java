package com.example.lotte.lottetest.Network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.lotte.lottetest.InfoManager;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.net.URLDecoder;

/**
 * Created by byungwoo on 2016-01-06.
 */
public class RegisterMember extends AsyncTask<String, Integer, String> {


    Context context;
    ////////////////////////////////////////////

    String json;

    String id;
    String password;
    String department;
    String name;

    public RegisterMember(Context context, String id, String password, String department, String name) {
        this.context = context;
        this.id = id;
        this.password = password;
        this.department = department;
        this.name = name;
    }

    @Override
    protected String doInBackground(String... params) {
        // 연결 시작


//        String url = "http://wkdgusdn3.dothome.co.kr/mamimap/review_list.php?store_id="+store_id;\

//        String url = "http://wkdgusdn3.dothome.co.kr/mamimap/register.php";
        String url = InfoManager.url + "user_register.php?id=1&password=1&department="+department+"&name="+name;

        OkHttpClient client = new OkHttpClient();

        if(password == null) {
            password = "";
        }

        try {

            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            json = response.body().string();    // 받아온 것 간순히 넣어주는 중..

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        String state=null;
        Log.e("json", json);
        try {
            JSONObject jsonObject = new JSONObject(json);


            state = URLDecoder.decode(jsonObject.getString("state"), "utf-8");


        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("state", state+"?");

        if(state.equals("success")) {
            Toast.makeText(context, "회원가입에 성공하셨습니다\n        로그인 해주세요.", Toast.LENGTH_SHORT).show();
        } else {
//            if(Session.getCurrentSession().checkAndImplicitOpen()) {
//                Log.e("세션..", "ㅁㅁ세션ㄴㅁㅁㅁ"+id_email);
//                ReceiveLogin task = new ReceiveLogin(context, id_email, null);
//                task.execute();

//                InfoManager.isLogin = true;
//                InfoManager.pre_activity.finish();
//                InfoManager.activity.finish();
//            } else {
                Toast.makeText(context, "회원가입에 실패하셨습니다.", Toast.LENGTH_SHORT).show();
//            }
        }

    }
}
