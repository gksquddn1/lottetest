package com.example.lotte.lottetest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


public class IntroLoginActivity extends AppCompatActivity {

    Button button_login_email;
    Button button_register;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_login);

        activity = this;

        setView();
        setListen();
    }

    void setView() {
        button_login_email = (Button)findViewById(R.id.login_email);
        button_register = (Button)findViewById(R.id.register_email);
    }

    void setListen() {
        button_login_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                InfoManager.activity = activity;

                startActivity(intent);
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);

                InfoManager.activity = activity;
                startActivity(intent);

            }
        });
    }

}