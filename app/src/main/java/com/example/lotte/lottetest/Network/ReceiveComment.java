package com.example.lotte.lottetest.Network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lotte.lottetest.Adapter.CommentAdapter;
import com.example.lotte.lottetest.InfoManager;
import com.example.lotte.lottetest.Model.Comment;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;

/**
 * Created by byungwoo on 2016-01-06.
 */
public class ReceiveComment extends AsyncTask<String, Integer, String> {


    Context context;
    ////////////////////////////////////////////
    CommentAdapter commentAdapter;

    String json;
    ListView list;
    String review_id;
    TextView result;

    public ReceiveComment(Context context, ListView list, String review_id, TextView result) {
        this.context = context;
        this.list = list;
        this.review_id = review_id;
        this.result = result;
    }

    @Override
    protected String doInBackground(String... params) {
        // 연결 시작

        commentAdapter = new CommentAdapter(context);

//        String url = "http://wkdgusdn3.dothome.co.kr/mamimap/around_store.php?latitude="+lati+"&longitude="+longi;
        //String url = "http://wkdgusdn3.dothome.co.kr/mamimap/around_store.php";
        String url = InfoManager.url + "comment.php?review_id=" + review_id;

        OkHttpClient client = new OkHttpClient();


        try {

            Request request = new Request.Builder()
                    .url(url)
                    .build();
//            Request request = new Request.Builder()
//                    .url(url)
//                    .build();

            Response response = client.newCall(request).execute();
            json = response.body().string();    // 받아온 것 간순히 넣어주는 중..
            Log.e("TAG", "????왜지"+json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Toast.makeText(context, "좌표" + saved_latitude + " : " + saved_longitude, Toast.LENGTH_SHORT).show();
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        Log.e("TAG", json+"?????????");
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray friendInfo = jsonObject.getJSONArray("comment");

            if (friendInfo.length() == 0) {
                list.setVisibility(View.GONE);
                result.setVisibility(View.VISIBLE);
            }

            for (int i = 0; i < friendInfo.length(); i++) {
                JSONObject infoObject = friendInfo.getJSONObject(i);

                String comment = URLDecoder.decode(infoObject.getString("comment"), "utf-8");
                String id = URLDecoder.decode(infoObject.getString("id"), "utf-8");
                Log.e("TAG", id + comment);

//
//
//                JSONArray imageInfo = infoObject.getJSONArray("image");
//
//                if(imageInfo.length() == 0) {
//                    // image의 정보가 없는 경우.. 근데 그럴 경우는 없음
//                }
//                ArrayList<String> image = new ArrayList<String>();
//                for(int j=0; j<imageInfo.length(); j++) {
////                    JSONObject imageObject = imageInfo.getJSONObject(j);
//                    image.add(URLDecoder.decode(imageInfo.getString(j), "utf-8"));
//                }


                Comment comment_info = new Comment(id, comment);

                commentAdapter.add(comment_info);

            }
        }catch (Exception e) {
            Log.e("ReceiveAround", e.toString());
            e.printStackTrace();
        }

        list.setAdapter(commentAdapter);
    }

}
