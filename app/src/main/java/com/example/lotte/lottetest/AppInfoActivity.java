package com.example.lotte.lottetest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.net.URLDecoder;

public class AppInfoActivity extends AppCompatActivity {
    ImageView appInfo_backBtn;

    TextView upgrade_information;
    TextView current_version;

    TextView question_owner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_info);

        setView();
        setListener();

        ReceiveVersion task = new ReceiveVersion(getApplicationContext());
        task.execute();

    }

    void setView() {
        appInfo_backBtn = (ImageView)findViewById(R.id.activity_appinfo_backBtn);
        upgrade_information = (TextView)findViewById(R.id.upgrade_information);
        current_version = (TextView)findViewById(R.id.current_version);
        question_owner = (TextView)findViewById(R.id.question_owner);
    }

    void setListener() {
        appInfo_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        question_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Uri uri = Uri.parse("mailto:greedeat@greedeat.com");

                Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                startActivity(intent);

            }
        });

    }

    class ReceiveVersion extends AsyncTask<String, String, String> {
        Context context;
        String json;

        public ReceiveVersion(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {

            String url = InfoManager.url + "app/version";

            OkHttpClient client = new OkHttpClient();

            try {
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Response response = client.newCall(request).execute();
                json = response.body().string();    // 받아온 것 간순히 넣어주는 중..

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            String version=null;
            SharedPreferences pref = getSharedPreferences("VERSION", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            try {
                JSONObject jsonObject = new JSONObject(json);
                version = URLDecoder.decode(jsonObject.getString("version"), "utf-8");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String save_version = pref.getString("new_version", null);


            if(save_version == null) {
                current_version.setText(version);
                editor.putString("now_version", version);
                upgrade_information.setText("최신버전 " + version);

            } else {
                current_version.setText(save_version);
                upgrade_information.setText("최신버전 " + version);
            }
            editor.commit();


        }
    }

}
